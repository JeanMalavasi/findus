import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

@Injectable()
export class ObjectsToLowerCasePipe implements PipeTransform {
   transform(value: Object, metadata: ArgumentMetadata) {
      return Object.fromEntries(Object.entries(value).map(
         ([key, value]) => [key, typeof value == 'string' ? value.toLowerCase().trim() : value]
      ));
   }
}

