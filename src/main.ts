import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './api/app.module';
import { ObjectsToLowerCasePipe } from './pipes/objectStringsToLowerCase.pipe';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors()
  instantiatePipes(app)
  await app.listen(process.env.PORT || 3000);
}
bootstrap();


function instantiatePipes(app: INestApplication) {
  app.useGlobalPipes(new ValidationPipe())
  app.useGlobalPipes(new ObjectsToLowerCasePipe())
}