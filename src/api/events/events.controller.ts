import { Controller, Get, Post, Body, Patch, Delete, HttpStatus, Query } from '@nestjs/common';
import { formatResponse } from 'src/helpers/format-response';
import { EVENT_RESPONSE } from 'src/helpers/responses/event';
import { CreateEventDto } from './dto/create-event.dto';
import { FetchManyEventDto } from './dto/fetch-many-event.dto';
import { FetchOneEventDto } from './dto/fetch-one-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventsService } from './events.service';


@Controller('event')
export class EventsController {
  constructor(private readonly service: EventsService) {}

  @Post()
  async createNewEvent(@Body() event: CreateEventDto) {
    const status = HttpStatus.CREATED;
    const message = EVENT_RESPONSE.CREATED_OK;
    const payload = await this.service.createNewEvent(event)

    return formatResponse(status, message, payload)
  }

  @Get()
  async fetchEventByFilters(@Query() filters: FetchManyEventDto) {
    const status = HttpStatus.OK;
    const message = EVENT_RESPONSE.FETCH_OK;

    const payload = await this.service.fetchEventByFilters(filters)

    return formatResponse(status, message, payload)
  }

  @Get('/one')
  async fetchEventById(@Query() filters: FetchOneEventDto) {
    const status = HttpStatus.OK;
    const message = EVENT_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchEventById(filters)

    return formatResponse(status, message, payload)
  }

  @Patch()
  async updateEventById(@Query() filter: FetchOneEventDto, @Body() event: UpdateEventDto) {
    const status = HttpStatus.OK;
    const message = EVENT_RESPONSE.UPDATED_OK;
    const payload = await this.service.updateEventById(filter, event)

    return formatResponse(status, message, payload)
  }

  @Delete()
  async removeEventById(@Query() filter: FetchOneEventDto) {
    const status = HttpStatus.OK;
    const message = EVENT_RESPONSE.REMOVE_OK;
    const payload = await this.service.removeEventById(filter)

    return formatResponse(status, message, payload)
  }
}
