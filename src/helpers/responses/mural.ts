export enum MURAL_RESPONSE {
   FETCH_OK = 'Mural fetched successfully',
   FETCH_NOK = 'Error fetching Mural',
 }