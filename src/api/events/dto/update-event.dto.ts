export class UpdateEventDto {
   name?: string
   description?: string
   day?: string
   hour?: string
   address?: {
      state?: string
      city?: string
      district?: string
      street?: string
      number?: number
      zipcode?: number
      complement?: string
   }
}
