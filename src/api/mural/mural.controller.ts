import { Controller, Get, Post, Body, Patch, Param, Delete, Query, HttpStatus } from '@nestjs/common';
import { MuralService } from './mural.service';
import { FetchOneMuralDto } from './dto/fetch-one-mural.dto';
import { MURAL_RESPONSE } from 'src/helpers/responses/mural';
import { formatResponse } from 'src/helpers/format-response';

@Controller('mural')
export class MuralController {
  constructor(private readonly service: MuralService) {}
  @Get('/one')
  async fetchMuralById(@Query() filters: FetchOneMuralDto) {
    const status = HttpStatus.OK;
    const message = MURAL_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchMuralById(filters)

    return formatResponse(status, message, payload)
  }
}
