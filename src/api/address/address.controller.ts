import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Query } from '@nestjs/common';
import { formatResponse } from 'src/helpers/format-response';
import { ADDRESS_RESPONSE } from 'src/helpers/responses/address';
import { AddressService } from './address.service';
import { FetchManyAddressDto } from './dto/fetch-many-user.dto';
import { FetchOneAddressDto } from './dto/fetch-one.dto';
import { UpdateAddressDto } from './dto/update-address.dto';

@Controller('address')
export class AddressController {
  constructor(private readonly service: AddressService) {}

  @Get()
  async fetchAddressByFilters(@Query() filters: FetchManyAddressDto) {
    const status = HttpStatus.OK;
    const message = ADDRESS_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchAddressByFilters(filters)

    return formatResponse(status, message, payload)
  }

  @Get('/one')
  async fetchAddressById(@Query() filters: FetchOneAddressDto) {
    const status = HttpStatus.OK;
    const message = ADDRESS_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchAddressById(filters)

    return formatResponse(status, message, payload)
  }

  @Patch()
  async updateAddressById(@Query() filters: FetchOneAddressDto, @Body() address: UpdateAddressDto) {
    const status = HttpStatus.OK;
    const message = ADDRESS_RESPONSE.UPDATED_OK;
    const payload = await this.service.updateAddressById(filters, address)

    return formatResponse(status, message, payload)
  }
}
