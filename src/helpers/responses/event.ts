export enum EVENT_RESPONSE {
   CREATED_OK = 'Event created successfully',
   CREATED_NOK = 'Error creating event',
 
   FETCH_OK = 'Event fetched successfully',
   FETCH_NOK = 'Error fetching event',
 
   UPDATED_OK = 'Event updated successfully',
   UPDATED_NOK = 'Error updating event',
 
   REMOVE_OK = 'Event delete successfully',
   REMOVE_NOK = 'Error deleting event'
 }