export class FetchManyAddressDto {
   state?: string
   city?: string
   district?: string
   street?: string
   number?: number
   zipcode?: number
   complement?: string
}