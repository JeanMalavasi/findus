import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { FetchManyUserDto } from './dto/fetch-many-user.dto';
import { FetchOneUserDto } from './dto/fetch-one.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserMapper } from './mappers/user-mapper';
import { UserRepository } from './repository';

@Injectable()
export class UsersService {
  constructor(private readonly repository: UserRepository) { }

  async registerUser(user: CreateUserDto) {
    const payload = await this.repository.registerUser(
      UserMapper.createToDatabase(user)
    )

    return UserMapper.fromDatabase(payload)
  }

  async fetchUsersByFilters(filters: FetchManyUserDto) {
    const payload = await this.repository.fetchUsersByFilters(
      UserMapper.fetchManyToDataBase(filters)
    )
      
    return payload.map(UserMapper.fromDatabase)
  }

  async fetchUserById(filter: FetchOneUserDto) {
    const payload = await this.repository.fetchUserById(
      UserMapper.fetchOneToDataBase(filter)
    )
    
    return UserMapper.fromDatabase(payload)
  }

  async updateUserById(filter: FetchOneUserDto, data: UpdateUserDto) {
    const payload = await this.repository.updateUserById(
      UserMapper.fetchOneToDataBase(filter), 
      UserMapper.updateToDatabase(data)
    )

    return UserMapper.fromDatabase(payload)
  }

  async removeUserById(filter: FetchOneUserDto,) {
    const payload = await this.repository.removeUserById(
      UserMapper.fetchOneToDataBase(filter)    
    )

    return UserMapper.fromDatabase(payload)
  }
}
