
import { IsString } from "class-validator"

export class CreateAddressDto {
   @IsString()
   state: string;

   @IsString()
   city: string;

   @IsString()
   district: string;

   @IsString()
   street: string;

   @IsString()
   number: number;

   @IsString()
   zipcode: number;
   
   @IsString()
   complement: string;
}