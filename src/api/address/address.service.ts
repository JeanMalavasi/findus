import { Injectable } from '@nestjs/common';
import { FetchManyAddressDto } from './dto/fetch-many-user.dto';
import { FetchOneAddressDto } from './dto/fetch-one.dto';
import { UpdateAddressDto } from './dto/update-address.dto';
import { AddressMapper } from './mappers/address-mapper';
import { AddressRepository } from './repository';

@Injectable()
export class AddressService {
  constructor(private readonly repository: AddressRepository) { }

  async fetchAddressByFilters(filters: FetchManyAddressDto) {
    const payload = await this.repository.fetchAddressByFilters(
      AddressMapper.fetchManyToDataBase(filters)
    )
    
    return payload.map(AddressMapper.fromDatabase)
  }

  async fetchAddressById(filter: FetchOneAddressDto) {
    const payload = await this.repository.fetchAddressById(
      AddressMapper.fetchOneToDataBase(filter)
    )

    return AddressMapper.fromDatabase(payload)
  }

  async updateAddressById(filter: FetchOneAddressDto, data: UpdateAddressDto) {
    const payload = await this.repository.updateAddressById(
      AddressMapper.fetchOneToDataBase(filter),
      AddressMapper.updateToDatabase(data)
    )

    return AddressMapper.fromDatabase(payload)
  }
}
