import { Module } from '@nestjs/common';
import { MuralService } from './mural.service';
import { MuralController } from './mural.controller';
import { MuralRepository } from './repository';
import { PrismaService } from '../app.service';

@Module({
  controllers: [MuralController],
  providers: [MuralService, MuralRepository, PrismaService]
})
export class MuralModule {}
