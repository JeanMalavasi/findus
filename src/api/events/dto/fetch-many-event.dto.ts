export class FetchManyEventDto {
   name?: string
   date?: string
   state?: string
   city?: string
   district?: string
   street?: string
   number?: number
   zipcode?: number
   tags?: string[]
}