export enum POST_RESPONSE {
  CREATED_OK = 'Post created successfully',
  CREATED_NOK = 'Error creating post',

  FETCH_OK = 'Post fetched successfully',
  FETCH_NOK = 'Error fetching post',

  UPDATED_OK = 'Post updated successfully',
  UPDATED_NOK = 'Error updating post',

  REMOVE_OK = 'Post delete successfully',
  REMOVE_NOK = 'Error deleting post'
}