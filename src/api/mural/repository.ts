import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { MURAL_RESPONSE } from 'src/helpers/responses/mural';
import { PrismaService } from '../app.service';
import { FetchOneMuralDto } from './dto/fetch-one-mural.dto';

@Injectable()
export class MuralRepository {
   private readonly logger = new Logger(MuralRepository.name);
   constructor(private prisma: PrismaService) { }

   async fetchUserById(filters: FetchOneMuralDto) {
      try {
         const payload = await this.prisma.mural.findFirst({
            where: {
               userId: filters.userId
            },
            include: {
               user: true,
               posts: true
            }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new NotFoundException(
            MURAL_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

}
