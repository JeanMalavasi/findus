import { Company, Client } from "@prisma/client"
import { IsString } from "class-validator"

export class CreateUserDto {
   @IsString()
   name: string

   @IsString()
   password: string

   @IsString()
   email: string

   @IsString()
   phone: string

   company?: Omit<Company, 'id' | 'userId'>

   client?: Omit<Client, 'id' | 'userId'>
}