import { BadRequestException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { USERS_RESPONSE } from 'src/helpers/responses/users';
import { PrismaService } from '../app.service';
import { CreateUserDto } from './dto/create-user.dto';
import { FetchManyUserDto } from './dto/fetch-many-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserRepository {
   private readonly logger = new Logger(UserRepository.name);
   constructor(private prisma: PrismaService) { }

   async validateContraints(data: UpdateUserDto) {
      if (data.client && data.company) {
         throw new BadRequestException('User cannot be both client and company at the same time',
            'User cannot be both client and company at the same time')
      }

      if (data.email) {
         const hasEmail = await this.fetchUsersByFilters({ email: data.email })
         if (hasEmail.length !== 0) {
            throw new BadRequestException('Email already exist',
               'Email already exist')
         }
      }

      if (data.phone) {
         const hasPhone = await this.fetchUsersByFilters({ phone: data.phone })
         if (hasPhone.length !== 0) {
            throw new BadRequestException('Phone already exist',
               'Phone already exist')
         }
      }

      if (data.company) {
         const hasCnpj = await this.fetchUsersByFilters({ cnpj: data.company.cnpj })
         if (hasCnpj.length !== 0) {
            throw new BadRequestException('CNPJ already exist',
               'CNPJ already exist')
         }
      }

      if (data.client) {
         const hasCpf = await this.fetchUsersByFilters({ cpf: data.client.cpf })
         if (hasCpf.length !== 0) {
            throw new BadRequestException('CPF already exist',
               'CPF already exist')
         }
      }

   }

   async registerUser(data: CreateUserDto) {
      try {
         await this.validateContraints(data)

         const { company, client, ...user } = data

         const payload = await this.prisma.user.create({
            data: {
               ...user,
               client: client && {
                  create: {
                     ...client
                  }
               },
               company: company && {
                  create: {
                     ...company
                  }
               },
               mural: {
                  create: {}
               }
            },
            include: { company: true, client: true, eventsCreated: true, eventParticipantIds: true, mural: true  }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            e.message ? e.message : USERS_RESPONSE.CREATED_NOK,
            e.message,
         );
      }
   }

   async fetchUsersByFilters(filters: FetchManyUserDto) {
      try {
         const payload = await this.prisma.user.findMany({
            where: {
               name: { contains: filters.name },
               email: { contains: filters.email },
               phone: { contains: filters.phone },
               company: { cnpj: filters.cnpj },
               client: { cpf: filters.cpf }
            },
            include: { company: true, client: true, eventsCreated: true, eventParticipantIds: true, mural: true  }
         })
         return payload

      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            USERS_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async fetchUserById(id: number) {
      try {
         const payload = await this.prisma.user.findUniqueOrThrow({
            where: {
               id
            },
            include: { company: true, client: true, eventsCreated: true, eventParticipantIds: true, mural: true  }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new NotFoundException(
            USERS_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async updateUserById(id: number, data: UpdateUserDto) {
      try {
         await this.validateContraints(data)
         const hasUser = await this.fetchUserById(id)

         const { company, client, ...user } = data
        
         const payload = await this.prisma.user.update({
            where: {
               id
            },
            data: {
               ...user,
               client: client && {
                  update: {
                     ...client
                  }
               },
               company: company && {
                  update: {
                     ...company
                  }
               }
            },
            include: { company: true, client: true, eventsCreated: true, eventParticipantIds: true, mural: true  }
         })

         return payload

      } catch (e) {
         this.logger.error(e);
         throw new BadRequestException(
            USERS_RESPONSE.UPDATED_NOK,
            e.message,
         );
      }
   }

   async removeUserById(id: number) {
      try {
         const hasUser = await this.fetchUserById(id)

         const payload = await this.prisma.user.delete({
            where: {
               id
            },
            include: { company: true, client: true, eventsCreated: true, eventParticipantIds: true, mural: true  }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new NotFoundException(
            USERS_RESPONSE.REMOVE_NOK,
            e.message,
         )
      }
   }
}
