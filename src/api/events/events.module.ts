import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { PrismaService } from '../app.service';
import { EventRepository } from './repository';

@Module({
  controllers: [EventsController],
  providers: [EventsService, EventRepository, PrismaService]
})
export class EventsModule {}
