export enum ADDRESS_RESPONSE {
   FETCH_OK = 'Address fetched successfully',
   FETCH_NOK = 'Error fetching address',
 
   UPDATED_OK = 'Address updated successfully',
   UPDATED_NOK = 'Error updating address',
 
   REMOVE_OK = 'Address delete successfully',
   REMOVE_NOK = 'Error deleting address'
 }