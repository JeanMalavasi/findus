
import { BadRequestException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { EVENT_RESPONSE } from 'src/helpers/responses/event';
import { PrismaService } from '../app.service';
import { CreateEventDto } from './dto/create-event.dto';
import { FetchManyEventDto } from './dto/fetch-many-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';

@Injectable()
export class EventRepository {
   private readonly logger = new Logger(EventRepository.name);
   constructor(private prisma: PrismaService) { }

   async createNewEvent(data: CreateEventDto) {

      const { address, ...event } = data

      const hasUser = await this.prisma.user.findUniqueOrThrow({ where: { id: event.creatorId } })

      const payload = await this.prisma.event.create({
         data: {
            name: event.name,
            description: event.description,
            day: event.day,
            hour: event.hour,
            creator: { connect: { id: event.creatorId } },
            address: {
               create: {
                  ...address
               }
            }
         },
         include: { address: true, creator: true, _count: {select: {participants: true}} }
      })


      return payload
   }

   async fetchEventByFilters(filters: FetchManyEventDto) {
      try {
         console.log(filters);
         
         const payload = await this.prisma.event.findMany({
            where: {
               name: { contains: filters.name },
               tags: filters.tags && {hasSome: filters.tags},
               address: {
                  state: { contains: filters.state },
                  city: { contains: filters.city },
                  district: { contains: filters.district },
                  number: filters.number,
                  zipcode: filters.zipcode,
               }
            },
            include: { address: true, creator: true, _count: {select: {participants: true}} }
         })
         return payload

      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            EVENT_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async fetchEventById(id: number) {
      try {
         const payload = await this.prisma.event.findFirstOrThrow({
            where: {
               id
            },
            include: { address: true, creator: true, _count: {select: {participants: true}} }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new NotFoundException(
            EVENT_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async updateEventById(id: number, data: UpdateEventDto) {
      try {
         console.log(data);
         
         const { address, ...event } = data

         const payload = await this.prisma.event.update({
            where: {
               id
            },
            data: {
               ...event,
               address: {
                  update: {
                     ...address
                  }
               }
            },
            include: { address: true, creator: true, _count: {select: {participants: true}} }
         })

         return payload
      } catch (e) {
         this.logger.error(e);
         throw new BadRequestException(
            EVENT_RESPONSE.UPDATED_NOK,
            e.message,
         );
      }
   }

   async removeEventById(id: number) {
      try {
         const hasEvent = await this.fetchEventById(id)

         const payload = await this.prisma.event.delete({
            where: {
               id
            },
            include: { address: true, creator: true, _count: {select: {participants: true}} }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new NotFoundException(
            EVENT_RESPONSE.REMOVE_NOK,
            e.message,
         )
      }
   }
}
