import { Injectable } from '@nestjs/common';
import { CreateEventDto } from './dto/create-event.dto';
import { FetchManyEventDto } from './dto/fetch-many-event.dto';
import { FetchOneEventDto } from './dto/fetch-one-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventMapper } from './mappers/event-mapper';
import { EventRepository } from './repository';

@Injectable()
export class EventsService {
  constructor(private readonly repository: EventRepository) { }

  async createNewEvent(event: CreateEventDto) {
    const payload = await this.repository.createNewEvent(
      EventMapper.createToDatabase(event)
    )
    
    return EventMapper.fromDatabase(payload)
  }

  async fetchEventByFilters(filters: FetchManyEventDto) {
    const payload = await this.repository.fetchEventByFilters(
      EventMapper.fetchManyToDataBase(filters)
    )

    return payload.map(EventMapper.fromDatabase)
  }

  async fetchEventById(filter: FetchOneEventDto) {
    const payload = await this.repository.fetchEventById(
      EventMapper.fetchOneToDataBase(filter)
    )

    return EventMapper.fromDatabase(payload)
  }

  async updateEventById(filter: FetchOneEventDto, event: UpdateEventDto) {
    const payload = await this.repository.updateEventById(
      EventMapper.fetchOneToDataBase(filter),
      EventMapper.updateToDatabase(event)
      )

      return EventMapper.fromDatabase(payload)
  }

  async removeEventById(filter: FetchOneEventDto) {
    const payload = await this.repository.removeEventById(
      EventMapper.fetchOneToDataBase(filter),
    )

    return EventMapper.fromDatabase(payload)
  }
}
