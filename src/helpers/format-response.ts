import { HttpStatus } from "@nestjs/common";

export function formatResponse(status: HttpStatus, message?: string, payload?: any) {
   return { status, message, payload }
 }