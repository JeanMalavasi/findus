import { Injectable } from '@nestjs/common';
import { FetchOneMuralDto } from './dto/fetch-one-mural.dto';
import { MuralMapper } from './mappers/mural-mapper';
import { MuralRepository } from './repository';


@Injectable()
export class MuralService {
  constructor(private readonly repository: MuralRepository) { }

  async fetchMuralById(filters: FetchOneMuralDto) {
    const payload = await this.repository.fetchUserById(
      MuralMapper.fetchOneToDataBase(filters)
    )
    
    return MuralMapper.fromDatabase(payload)
  }
}
