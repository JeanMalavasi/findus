import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { USERS_RESPONSE } from 'src/helpers/responses/users';
import { formatResponse } from 'src/helpers/format-response';
import { FetchManyUserDto } from './dto/fetch-many-user.dto';
import { FetchOneUserDto } from './dto/fetch-one.dto';

@Controller('user')
export class UsersController {
  constructor(private readonly service: UsersService) { }

  @Post()
  async registerUser(@Body() user: CreateUserDto) {
    const status = HttpStatus.CREATED;
    const message = USERS_RESPONSE.CREATED_OK;
    const payload = await this.service.registerUser(user)

    return formatResponse(status, message, payload)
  }

  @Get()
  async fetchUsersByFilters(@Query() filters: FetchManyUserDto) {
    const status = HttpStatus.OK;
    const message = USERS_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchUsersByFilters(filters)

    return formatResponse(status, message, payload)
  }

  @Get('/one')
  async fetchUserById(@Query() filters: FetchOneUserDto) {
    const status = HttpStatus.OK;
    const message = USERS_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchUserById(filters)

    return formatResponse(status, message, payload)
  }

  @Patch()
  async updateUserById(@Query() filters: FetchOneUserDto, @Body() user: UpdateUserDto) {
    const status = HttpStatus.OK;
    const message = USERS_RESPONSE.UPDATED_OK;
    const payload = await this.service.updateUserById(filters, user)

    return formatResponse(status, message, payload)
  }

  @Delete()
  async removeUserById(@Query() filters: FetchOneUserDto) {
    const status = HttpStatus.OK;
    const message = USERS_RESPONSE.REMOVE_NOK;
    const payload = await this.service.removeUserById(filters)

    return formatResponse(status, message, payload)
  }
}
