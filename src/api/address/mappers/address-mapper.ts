import { Address } from "@prisma/client";
import { FetchManyAddressDto } from "../dto/fetch-many-user.dto";
import { FetchOneAddressDto } from "../dto/fetch-one.dto";
import { UpdateAddressDto } from "../dto/update-address.dto";


export class AddressMapper {
   public static updateToDatabase(address: UpdateAddressDto) {
      return {
         state: address.state,
         city: address.city,
         district: address.district,
         street: address.street,
         number: address.number,
         zipcode: address.zipcode ,
         complement: address.complement
      }
   }

   public static fromDatabase(address: Address) {
      return {
         id: address.id,
         state: address.state,
         city: address.city,
         district: address.district,
         street: address.street,
         number: address.number,
         zipcode: address.zipcode,
         complement: address.complement
      }
   }

   public static fetchManyToDataBase(filters: FetchManyAddressDto) {
      return {
         state: filters.state,
         city: filters.city,
         district: filters.district,
         street: filters.street,
         number: +filters.number,
         zipcode: +filters.zipcode,
         complement: filters.complement
      }
   }

   public static fetchOneToDataBase(filter: FetchOneAddressDto) {
      return +filter.id
   }
}