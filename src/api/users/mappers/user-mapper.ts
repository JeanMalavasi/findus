import { CreateUserDto } from "../dto/create-user.dto";
import { UpdateUserDto } from "../dto/update-user.dto";
import { Client, Company, User, Event, Mural } from "@prisma/client";
import { FetchManyUserDto } from "../dto/fetch-many-user.dto";
import { FetchOneUserDto } from "../dto/fetch-one.dto";

export class UserMapper {
   public static createToDatabase(user: CreateUserDto) {
      return {
         name: user.name,
         password: user.password,
         email: user.email,
         phone: user.phone,
         client: user.client,
         company: user.company,
      }
   }

   public static updateToDatabase(user: UpdateUserDto) {
      return {
         name: user.name,
         password: user.password,
         email: user.email,
         phone: user.phone,
         client: user.client,
         company: user.company
      }
   }

   public static fromDatabase(user: User & {
      company: Company;
      client: Client;
      eventsCreated: Event[];
      eventParticipantIds: Event[];
      mural: Mural;
  }) {
      return {
         id: user.id,
         name: user.name,
         email: user.email,
         phone: user.phone,
         client: user.client || undefined,
         company: user.company || undefined,
         eventsCreated: user.eventsCreated,
         eventParticipantIds: user.eventParticipantIds,
         mural: user.mural
      }
   }

   public static fetchManyToDataBase(filters: FetchManyUserDto) {
      return {
         name: filters.name,
         email: filters.email,
         phone: filters.phone,
         cnpj: filters.cnpj,
         cpf: filters.cpf
      }
   }

   public static fetchOneToDataBase(filter: FetchOneUserDto) {
      return +filter.id
   }
}