import { BadRequestException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { POST_RESPONSE } from 'src/helpers/responses/post';
import { PrismaService } from '../app.service';
import { CreatePostDto } from './dto/create-post.dto';
import { FetchManyPostDto } from './dto/fetch-many-user.dto';
import { FetchOnePostDto } from './dto/fetch-one.dto';
import { UpdatePostDto } from './dto/update-post.dto';


@Injectable()
export class PostRepository {
   private readonly logger = new Logger(PostRepository.name);
   constructor(private prisma: PrismaService) { }

   async createNewPost(data: CreatePostDto) {
      try {
         const payload = await this.prisma.post.create({
            data: {
               ...data
            }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            POST_RESPONSE.CREATED_NOK,
            e.message,
         );
      }
   }

   async fetchPostsByFilters(filters: FetchManyPostDto) {
      try {
         const payload = await this.prisma.post.findMany({
            where: {
               ...filters
            }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            POST_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async fetchPostById(filters: FetchOnePostDto) {
      try {
         const payload = await this.prisma.post.findFirstOrThrow({
            where: {
               id: filters.id,
               muralId: filters.muralId
            }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            POST_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async updatePostById(filters: FetchOnePostDto, data: UpdatePostDto) {
      try {
         const hasPost = await this.fetchPostById(filters)
         
         const payload = await this.prisma.post.update({
            where: {
               id: filters.id,
            },
            data: {
               ...data
            }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            POST_RESPONSE.UPDATED_NOK,
            e.message,
         );
      }
   }

   async removePostById(filters: FetchOnePostDto) {
      try {
         const hasPost = await this.fetchPostById(filters)
         
         const payload = await this.prisma.post.delete({
            where: {
               id: filters.id
            }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            POST_RESPONSE.REMOVE_NOK,
            e.message,
         );
      }
   }
}
