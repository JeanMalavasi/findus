import { IsString } from "class-validator";

export class FetchOneEventDto {
   @IsString()
   id: number
}