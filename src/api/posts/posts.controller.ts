import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Query } from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { POST_RESPONSE } from 'src/helpers/responses/post';
import { formatResponse } from 'src/helpers/format-response';
import { FetchManyPostDto } from './dto/fetch-many-user.dto';
import { FetchOnePostDto } from './dto/fetch-one.dto';

@Controller('post')
export class PostsController {
  constructor(private readonly service: PostsService) {}

  @Post()
  async createNewPost(@Body() post: CreatePostDto) {
    const status = HttpStatus.CREATED;
    const message = POST_RESPONSE.CREATED_OK;
    const payload = await this.service.createNewPost(post)

    return formatResponse(status, message, payload)
  }

  @Get()
  async fetchPostsByFilters(@Query() filters: FetchManyPostDto) {
    const status = HttpStatus.OK;
    const message = POST_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchPostsByFilters(filters)

    return formatResponse(status, message, payload)
  }

  @Get('/one')
  async fetchPostById(@Query() filters: FetchOnePostDto) {
    const status = HttpStatus.OK;
    const message = POST_RESPONSE.FETCH_OK;
    const payload = await this.service.fetchPostById(filters)

    return formatResponse(status, message, payload)
  }

  @Patch()
  async updatePostById(@Query() filters: FetchOnePostDto, @Body() user: UpdatePostDto) {
    const status = HttpStatus.OK;
    const message = POST_RESPONSE.UPDATED_OK;
    const payload = await this.service.updatePostById(filters, user)

    return formatResponse(status, message, payload)
  }

  @Delete()
  async removePostById(@Query() filters: FetchOnePostDto) {
    const status = HttpStatus.OK;
    const message = POST_RESPONSE.REMOVE_NOK;
    const payload = await this.service.removePostById(filters)

    return formatResponse(status, message, payload)
  }
}
