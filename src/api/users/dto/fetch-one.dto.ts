import { IsString } from "class-validator";

export class FetchOneUserDto {
   @IsString()
   id: number
}