
import { Mural, Post, User } from "@prisma/client";
import { FetchOneMuralDto } from "../dto/fetch-one-mural.dto";

export class MuralMapper {
   public static fromDatabase(mural: Mural & {
      user: User;
      posts: Post[];
  }) {
      return {
         id: mural.id,
         user: mural.user,
         posts: mural.posts
      }
   }

   public static fetchOneToDataBase(filter: FetchOneMuralDto) {
      return {
         userId: +filter.userId
      }
   }
}