import { IsNumber, IsString } from "class-validator";

export class CreatePostDto {
   @IsString()
   message: string;

   @IsNumber()
   muralId: number
}
