import { Address, Event, User } from "@prisma/client"
import { FetchManyEventDto } from "../dto/fetch-many-event.dto";
import { FetchOneEventDto } from "../dto/fetch-one-event.dto";
import { UpdateEventDto } from "../dto/update-event.dto";
import { CreateEventDto } from "../dto/create-event.dto";

export class EventMapper {
   public static createToDatabase(event: CreateEventDto) {
      return {
         name: event.name,
         description: event.description,
         day: event.day,
         hour: event.hour,
         creatorId: +event.creatorId,
         address: event.address
      }
   }

   public static updateToDatabase(event: UpdateEventDto) {
      return {
         name: event.name,
         description: event.description,
         day: event.day,
         hour: event.hour,
         address: {...event.address, number: event.address.number && +event.address.number, zipcode: event.address.zipcode && +event.address.zipcode},
      }
   }

   public static fromDatabase(event: Event & {
      address: Address;
      creator: User;
      _count: {
         participants: number;
     }
  }) {
      return {
         id: event.id,
         name: event.name,
         description: event.description,
         day: event.day,
         hour: event.hour,
         tags: event.tags,
         creatorName: event.creator.name,
         participantsCount: event._count.participants,
         address: {...event.address, latitude: +event.address.latitude, longitude: +event.address.longitude},
      }
   }

   public static fetchManyToDataBase(filters: FetchManyEventDto) {
      return {
         name: filters.name,
         date: filters.date,
         state: filters.state,
         city: filters.city,
         district: filters.district,
         street: filters.street,
         number: filters.number && +filters.number,
         zipcode: filters.zipcode && +filters.zipcode,
         tags: filters.tags && JSON.parse(filters.tags as unknown as string),
      }
   }

   public static fetchOneToDataBase(filter: FetchOneEventDto) {
      return +filter.id
   }
}