
import { BadRequestException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { ADDRESS_RESPONSE } from 'src/helpers/responses/address';
import { PrismaService } from '../app.service';
import { FetchManyAddressDto } from './dto/fetch-many-user.dto';
import { UpdateAddressDto } from './dto/update-address.dto';


@Injectable()
export class AddressRepository {
   private readonly logger = new Logger(AddressRepository.name);
   constructor(private prisma: PrismaService) { }

   async validateUniqueContraints(data: UpdateAddressDto) {
      if (data.city && data.complement && data.district && data.number && data.state && data.street && data.zipcode) {
         const hasAddress = await this.fetchAddressByFilters({ ...data })
         if (hasAddress.length !== 0) {
            throw new BadRequestException('Address already exist',
               'Address already exist')
         }
      }
   }

   async fetchAddressByFilters(filters: FetchManyAddressDto) {
      try {
         const payload = await this.prisma.address.findMany({
            where: {
               state: { contains: filters.state },
               city: { contains: filters.city },
               district: { contains: filters.district },
               complement: { contains: filters.complement },
               number: filters.number,
               zipcode: filters.zipcode
            },
         })
         return payload
         
      } catch (e) {
         this.logger.error(e)
         throw new BadRequestException(
            ADDRESS_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async fetchAddressById(id: number) {
      try {
         const payload = await this.prisma.address.findUniqueOrThrow({
            where: {
               id
            }
         })

         return payload
      } catch (e) {
         this.logger.error(e)
         throw new NotFoundException(
            ADDRESS_RESPONSE.FETCH_NOK,
            e.message,
         );
      }
   }

   async updateAddressById(id: number, data: UpdateAddressDto) {
      try {
         await this.validateUniqueContraints(data)

         const hasAddress = await this.fetchAddressById(id)

         const payload = await this.prisma.address.update({
            where: {
               id
            },
            data
         })

         return payload
      } catch (e) {
         this.logger.error(e);
         throw new BadRequestException(
            ADDRESS_RESPONSE.UPDATED_NOK,
            e.message,
         );
      }
   }
}
