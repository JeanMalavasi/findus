import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import { AddressController } from './address.controller';
import { PrismaService } from '../app.service';
import { AddressRepository } from './repository';

@Module({
  controllers: [AddressController],
  providers: [AddressService, AddressRepository, PrismaService]
})
export class AddressModule {}
