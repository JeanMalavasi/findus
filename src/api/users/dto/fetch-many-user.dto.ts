export class FetchManyUserDto {
   name?: string
   email?: string
   phone?: string
   cnpj?: string
   cpf?: string
}