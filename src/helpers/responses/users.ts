export enum USERS_RESPONSE {
   CREATED_OK = 'User created successfully',
   CREATED_NOK = 'Error creating user',
 
   FETCH_OK = 'User fetched successfully',
   FETCH_NOK = 'Error fetching user',
 
   UPDATED_OK = 'User updated successfully',
   UPDATED_NOK = 'Error updating user',
 
   REMOVE_OK = 'User delete successfully',
   REMOVE_NOK = 'Error deleting user'
 }