import { IsString } from "class-validator";

export class FetchOneAddressDto {
   @IsString()
   id: number
}