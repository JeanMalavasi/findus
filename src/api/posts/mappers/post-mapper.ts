
import { Client, Company, User, Event, Mural, Post } from "@prisma/client";
import { CreatePostDto } from "../dto/create-post.dto";
import { FetchManyPostDto } from "../dto/fetch-many-user.dto";
import { FetchOnePostDto } from "../dto/fetch-one.dto";
import { UpdatePostDto } from "../dto/update-post.dto";


export class PostMapper {
   public static createToDatabase(post: CreatePostDto) {
      return {
         message: post.message,
         muralId: +post.muralId
      }
   }

   public static updateToDatabase(post: UpdatePostDto) {
      return {
         message: post.message
      }
   }

   public static fromDatabase(user: Post) {
      return {
         id: user.id,
         message: user.message,
         createdAt: user.createdAt,
         muralId: user.muralId
      }
   }

   public static fetchManyToDataBase(filters: FetchManyPostDto) {
      return {}
   }

   public static fetchOneToDataBase(filter: FetchOnePostDto) {
      return { id: filter.id && +filter.id, muralId: filter.muralId && +filter.muralId }
   }
}