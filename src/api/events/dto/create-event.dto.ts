import { Prisma, User } from "@prisma/client";
import { IsDateString, IsJSON, IsNumber, IsString } from "class-validator";

export class CreateEventDto {
   @IsString()
   name: string

   @IsString()
   description: string

   @IsNumber()
   creatorId: number

   @IsString()
   day: string
   
   @IsString()
   hour: string


   address: {
      state: string
      city: string
      district: string
      street: string
      number: number
      zipcode: number
      complement: string
      latitude: Prisma.Decimal
      longitude: Prisma.Decimal
   }
}


