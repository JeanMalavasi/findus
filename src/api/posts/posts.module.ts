import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { PostRepository } from './repository';
import { PrismaService } from '../app.service';

@Module({
  controllers: [PostsController],
  providers: [PostsService, PostRepository, PrismaService]
})
export class PostsModule {}
