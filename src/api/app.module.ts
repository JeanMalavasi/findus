import { Module } from '@nestjs/common';
import { PrismaService } from './app.service';
import { UsersModule } from './users/users.module';
import { AddressModule } from './address/address.module';
import { EventsModule } from './events/events.module';
import { MuralModule } from './mural/mural.module';
import { PostsModule } from './posts/posts.module';

@Module({
  imports: [UsersModule, AddressModule, EventsModule, MuralModule, PostsModule],
  providers: [PrismaService],
})
export class AppModule {}
