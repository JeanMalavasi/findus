export class UpdateUserDto {
   name?: string
   password?: string
   email?: string
   phone?: string
   company?: {
      cnpj?: string;
      activitBranch?: string;
      description?: string;
   }
   client?: {
      cpf?: string;
      genre?: string;
   }
}