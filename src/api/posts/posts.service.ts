import { Injectable } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { FetchManyPostDto } from './dto/fetch-many-user.dto';
import { FetchOnePostDto } from './dto/fetch-one.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { PostMapper } from './mappers/post-mapper';
import { PostRepository } from './repository';

@Injectable()
export class PostsService {
  constructor(private readonly repository: PostRepository) { }

  async createNewPost(post: CreatePostDto) {
    const payload = await this.repository.createNewPost(
      PostMapper.createToDatabase(post)
    )

    return PostMapper.fromDatabase(payload)
  }

  async fetchPostsByFilters( filters: FetchManyPostDto) {
    const payload = await this.repository.fetchPostsByFilters(
      PostMapper.fetchManyToDataBase(filters)
    )

    return payload.map(PostMapper.fromDatabase)
  }

  async fetchPostById(filters: FetchOnePostDto) {
    const payload = await this.repository.fetchPostById(
      PostMapper.fetchOneToDataBase(filters)
    )

    return PostMapper.fromDatabase(payload)

  }

  async updatePostById(filters: FetchOnePostDto, post: UpdatePostDto) {
    const payload = await this.repository.updatePostById(
      PostMapper.fetchOneToDataBase(filters),
      PostMapper.updateToDatabase(post)
    )

    return PostMapper.fromDatabase(payload)
  }

  async removePostById(filters: FetchOnePostDto) {
    const payload = await this.repository.removePostById(
      PostMapper.fetchOneToDataBase(filters)
    )

    return PostMapper.fromDatabase(payload)
  }
}
